﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script handles GUI disabling/enabling based on current GameState;
public class MainUIController : MonoBehaviour {

    public RectTransform titleScreenGUI, gamePlayGUI, gameOverUI;

    // Update is called once per frame
    void Update() {

        if (GameManager.gm.state == GameManager.GameState.ready)
        {
            titleScreenGUI.gameObject.SetActive(false);
            gamePlayGUI.gameObject.SetActive(false);
        }
        else if (GameManager.gm.state == GameManager.GameState.normalPlay || GameManager.gm.state == GameManager.GameState.wallEvent)
        {
            titleScreenGUI.gameObject.SetActive(false);
            gamePlayGUI.gameObject.SetActive(true);
        }
        else if(GameManager.gm.state == GameManager.GameState.gameOver)
        {
            gameOverUI.gameObject.SetActive(true);
        }
        else
        {
            gamePlayGUI.gameObject.SetActive(false);
        }

        if(GameManager.gm.state == GameManager.GameState.menu)
        {
            titleScreenGUI.gameObject.SetActive(true);
        }
    }
		
     

	
}
