using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachedObstacle : MonoBehaviour 
{
	public GameObject wall;
	//Get reference to this object's transform/rigidbody for efficiency.
	private Transform myTransform;
	private Vector3 startPosition;
	private Rigidbody myBody;

	// Use this for initialization
	void Start()
	{
		//Assign current game object's transform.
		Vector3 temp = new Vector3(wall.transform.position.x, transform.position.y, transform.position.z);
		transform.position = temp;
		myTransform = transform;

		myBody = gameObject.GetComponent<Rigidbody>();
		startPosition = transform.position;

	}

	void OnEnable()
	{
		Vector3 temp = new Vector3(wall.transform.position.x, transform.position.y, transform.position.z);
		transform.position = temp;
	}

	// Update is called once per frame
	void Update () {

		//Update velocity based on value held by game manager;
		myBody.velocity = new Vector3(0f, 0.5f, 0f) * GameManager.gm.getGlobalMoveSpeed();

	}

	void OnDisable()
	{
		myTransform.position = startPosition;
	}
}
