﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour 
{

	//starting, gameplay position, zoomed pos for gameOver state
	private Vector3 startPos, zoomPos;

	//Player object to get location
	private GameObject player;

	//This object's transform
	private Transform myTransform;

	//coroutune occupied
	private bool occupied = false;



	// Use this for initialization
	void Start()
	{
		myTransform = transform;

		startPos = transform.position;

		player = GameObject.FindGameObjectWithTag("Player");

	}


	// Update is called once per frame
	void Update () {

		if (GameManager.gm.state == GameManager.GameState.gameOver)
		{
			if(!occupied)
			{

				StartCoroutine(_zoomIn());
			}
		}
		else
		{
			if (!occupied && myTransform.position != startPos)
			{

				StartCoroutine(_zoomOut());
			}
		}

	}

	public IEnumerator _zoomIn()
	{
		occupied = true;

		zoomPos = new Vector3(player.transform.position.x, player.transform.position.y, 3f);
		float timeToStart = Time.time;
		while(myTransform.position != zoomPos)
		{
			myTransform.position = Vector3.Lerp(myTransform.position, zoomPos, 0.5f * (Time.time - timeToStart));

			yield return null;
		}

		occupied = false;
	}

	public IEnumerator _zoomOut()
	{
		occupied = true;

		float timeToStart = Time.time;
		while (myTransform.position != startPos)
		{
			myTransform.position = Vector3.Lerp(myTransform.position, startPos, 0.5f * (Time.time - timeToStart));

			yield return null;
		}

		occupied = false;
	}

}
