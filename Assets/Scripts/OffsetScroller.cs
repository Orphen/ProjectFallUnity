﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetScroller : MonoBehaviour {

	private GameManager gameManager;
	private Renderer rend;

	//SpeedOffset: a multiplier used to determine wallscrolling speed.
	public float speedOffset;
	//ScrollSpeed: the scrolling speed of the walls.
	public float scrollSpeed = -0.1f;
	//targetSpeed: the updated speed from the game manager to be adjusted to.
	private float targetSpeed;
	//texOffset: the texture offset of the material main texture.
	private float texOffset;
	//damp speed: adjusts the speed that scrolling will adjust to the target speed.
	private float dampSpeed = 1.0f;

	//saved offset: the default offset of the material main texture.
	private Vector2 savedOffset;

	// Use this for initialization
	void Start () 
	{
		GameObject gameManagerObject = GameObject.FindWithTag ("GameManager");
		if (gameManagerObject != null) {
			gameManager = gameManagerObject.GetComponent<GameManager> ();
		}
		if (gameManager == null) {
			Debug.Log ("Cannot find 'GameManager' script");
		}

		targetSpeed = scrollSpeed;

		rend = gameObject.GetComponent<Renderer> ();
		savedOffset = rend.sharedMaterial.GetTextureOffset ("_MainTex");		
	}

	// Update is called once per frame
	void Update () 
	{
		//receive updated target speed from the game manager. 
		//adjust the speed by the speed offset
		targetSpeed = -(gameManager.getGlobalMoveSpeed() * speedOffset);

		//slowly adjust or damps speed to the target speed
		//adjust the damping speed 
		scrollSpeed += (targetSpeed - scrollSpeed) * dampSpeed;

		//calculates the amount to add to offset based on scroll speed.
		texOffset += scrollSpeed * Time.deltaTime;

		//normalizes the texture offset to values between 0 and 1 to avoid large negative offsets.
		float y = Mathf.Repeat (texOffset, 1);

		//Sets the offset of the material's main texture.
		Vector2 offset = new Vector2 (savedOffset.x, y);
		rend.sharedMaterial.SetTextureOffset ("_MainTex", offset);
	}

	void OnDisable(){
		rend.sharedMaterial.SetTextureOffset ("_MainTex", savedOffset);
	}
}
