﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

//Script used for ingame break meter
public class StatusIndicator : MonoBehaviour {

    [SerializeField]
    private RectTransform breakMeterBarRect; //Rect img pass in through inspector

    //Player reference to get recharge status
    Player player;

	// Use this for initialization
	void Start () {

        if(breakMeterBarRect == null)
        {
            Debug.LogError("Status indicator: No breakmeter bar object referenced");
        }

        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setBrakeBar(int _cur, int _max)
    {
        float _value = (float)_cur / _max;
        breakMeterBarRect.localScale = new Vector3(_value, breakMeterBarRect.localScale.y, breakMeterBarRect.localScale.z);

        if(_value <= .45f && !player.isRecharging())
        {
            breakMeterBarRect.gameObject.GetComponent<Image>().color = Color.yellow;
        }
        else if(_value  >= .45 && !player.isRecharging())
        {
            breakMeterBarRect.gameObject.GetComponent<Image>().color = Color.green;
        }
        else //Is recharging
        {
            breakMeterBarRect.gameObject.GetComponent<Image>().color = Color.red;
        }


    }
}
