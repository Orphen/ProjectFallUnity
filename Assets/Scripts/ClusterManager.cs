﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClusterManager : MonoBehaviour {

	//Array of cluster objects, size set in inspector;
	public GameObject[] cluster;

    public GameObject highScoreCluster;

    public bool highScoreClusterSpawned = false; //Only activate once per game session




	//Keep track of current active cluster;
	bool activeCluster;

	// Use this for initialization
	void Start () {

		activeCluster = true;
	}

	// Update is called once per frame
	void Update() {

		//If all clusters disabled, find new cluster
		if (allClustersDisabled())
		{
			if (GameManager.gm.state == GameManager.GameState.normalPlay) {
				findNewCluster ();
			}

		}

        //Only spawn highscore cluster once, during normal play
        if(GameManager.gm.getScore() >= GameManager.gm.getHighscore() && !highScoreClusterSpawned && GameManager.gm.state == GameManager.GameState.normalPlay)
        {
            highScoreClusterSpawned = true;
            highScoreCluster.SetActive(true);
        }

		if(GameManager.gm.state == GameManager.GameState.ready)
		{
			highScoreClusterSpawned = false;
		}

    
	}

	public bool allClustersDisabled()
	{
		foreach (GameObject o in cluster)
		{
			if(o.activeInHierarchy)
			{
				return false; //an object was found active, return false;
			}
		}

		return true; //all objects returned inactive, return true;
	}

	public void findNewCluster()
	{
		//Find new cluster
		int selection = Random.Range(0, cluster.Length);

		//enable the new cluster
		cluster[selection].SetActive(true);

	}

}