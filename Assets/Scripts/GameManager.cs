﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour 
{

	//Static variable single instance of GameManager reference
	public static GameManager gm;

	//Ad Stuff
	public AdController adcontroller;
	public int adCooldown;
	private int adCount;

	//Reference to player to get breaking recharge status;
    private Player playerScriptRef;

	//Enum for gamestates
	public enum GameState
	{
		test, //testing purposes
		menu, //Menu state for the menu
		setup, //Setup phase, if needed
		results, //Resul;ts phase to show gameOver results
		ready, //Ready state to last n seconds before moving to normalPlay (For animations)
		normalPlay, //Normal gameplay state
        wallEvent, //Wall closing event state
		waiting, //waiting state for any transitions into gameplay (For animations)
        returnToTitle, //waitingState for any transitions to the TitleScreen (For animations)
        gameOver //Gameover state
	}

    ///////////EVENT BLOCK//////////////
    //Event cooldown flag to prevent concurrent events//
    public bool eventCooldown;
    //Other event flags//
    private int wallEvent_selection; //random int between 1 and 3 to determine which wall will close in
    private float distance_EventOccur; //What the score was during the last occurrence of the wallEvent
    [SerializeField]
    [Range(300, 500)]
    private int firstEvent_threshold; //score which triggers the first event, set in inspector
    [SerializeField]
    [Range(3500, 7500)]
    private int min_EventThreshold, max_EventThreshold; //min and mix difference in score from last occurrence can be before next event occurs, set in inspector
    private int reoccurThreshold; //Random between min and max thresholds, assigned everytime event occurs
    /////////////////////////////////////


    //Current and previos state enums
    public GameState state;
	public GameState prevState;
    public GameState diedAtState;

	//Score
	[SerializeField]
	private int highScore, score, multiplier; //Score to be recorded as a measure of distance;
	private float timeToStart, distance, lastBrake, lastSpeedBump; //For keeping track of when game actually starts to add score
    [SerializeField]
    private Text highScoreVisualObject; //Set in inspector
	//Keep track of these variables for gameplay;
	//The player, set in inspector
	[SerializeField]
	public GameObject player;
	//The global moveSpeed for obstacles to scroll up, set in inspector
	[SerializeField]
	private float globalMoveSpd;
	[SerializeField]
	private float globalMaxSpd;
	[SerializeField]
	private float brakingSpd;
	[SerializeField]
	private float targetSpd;

	//Time to decelerate to brakingSpd;
	[SerializeField]
	private float decDuration;
	//Time to accelerate to maxSpd;
	[SerializeField]
	private float accDuration;
	//starting value for the accleration Lerp;
	private static float startTime;

	//braking flag;
	public bool braking;
	//gameOver flag
	public bool gameOver;
	//coroutine occupied for acceleration, occupied2 for deceleration
	public bool occupied, occupied2 = false;

    //coroutine handling for state transitioning
    public bool stateOccupied = false;
    //State transition variables;
    public float delayBeforeReady; //Delay in seconds before game starts, set in inspector

    //Debug//
    [SerializeField]
    private bool keyboardControls; //For testing with keyboard vs mouse controls, set in inspector
    [SerializeField]
    private bool phoneControls; //For testing with keyboard vs mouse controls, set in inspector
    [SerializeField]
    private bool mouseControls; //For testing with keyboard vs mouse controls, set in inspector


    // Use this for initialization
    void Start() {
		targetSpd = globalMaxSpd;

		score = 0;
		distance = 0;
		lastBrake = 0;
		lastSpeedBump = 0;
		multiplier = 1;
		adCount = 0;

	}

	void Awake()
	{
		//Check if gm has not been instantiated, if not instantiate it to this gameObject;
		if (gm != null)
		{
			Debug.LogError("More than one GameManager in scene");
		}
		else
		{
			gm = this;
		}
        
        //For use of cheecking break recharge status;
        playerScriptRef = player.GetComponent<Player>();

        //Disable all clusters if not in normalPlay mode;
        if(state != GameState.normalPlay)
        {
            disableAllClusters();
        }


        //Load highscore and other prefs
        loadPrefs();

        prevState = state;

        eventCooldown = false;

	}

	// Update is called once per frame
	void Update() {

        //Temp, delete and move later
        highScoreVisualObject.text = "Best: " + PlayerPrefs.GetInt("Highscore").ToString();

        //Handle game speed velocity and player input;
        handleSpeedAndInput();

        //State changes//
        handleStateChanges();
	}

	//Get method for access
	public float getGlobalMoveSpeed()
	{
		return globalMoveSpd;
	}

    public void disableAllClusters()
    {
        GameObject[] clusters = GameObject.FindGameObjectsWithTag("Cluster");

        foreach (GameObject c in clusters)
        {
            c.SetActive(false);
        }
    }


	public IEnumerator setspeedDown(float initial)
	{
		occupied2 = true;
		float elapsed = 0;
		//While elapsed time is less than duration, increment
		while (elapsed < decDuration && braking)
		{     
			//Must assign value of initialSpeed, which is the speed at which breaking starts
			globalMoveSpd = Mathf.Lerp(initial, targetSpd, elapsed / decDuration);
			elapsed += Time.deltaTime;

			yield return null;
		}

		occupied2 = false;
	}

	public IEnumerator setspeedUp(float initial)
	{
		occupied = true;
		float elapsed = 0;
		//While elapsed time is less than duration, increment
		while (elapsed < accDuration && !braking && !gameOver)
		{
			//Must assign value of initialSpeed, which is the speed at which acceleration starts
			globalMoveSpd = Mathf.Lerp(initial, targetSpd, elapsed / accDuration);
			elapsed += Time.fixedDeltaTime;

			yield return null;
		} 

		occupied = false;
	}

    public int getScore()
    {
        return score;
    }

	public int getMultiplier()
	{
		return multiplier;
	}

    //Handles game speed velocity
    public void handleSpeedAndInput()
    {
        //player moves only during normal play or testing
        if (state == GameState.normalPlay || state == GameState.test || state == GameState.menu || state == GameState.ready || state == GameState.wallEvent)
        {

            incrementScore();

            //Slow down
            if (playerScriptRef.isBraking() && !playerScriptRef.isRecharging())
            {
                //Started braking
                braking = true;
                //Set targetSpd to brakingSpd;
                targetSpd = brakingSpd;

				lastBrake = distance;
				multiplier = 1;

                //If the couritine is not busy, and the globalSpd has not reached brakingSpd
                if (!occupied2 && (globalMoveSpd > brakingSpd))
                {
                    //Pass in speed at which braking has started;
                    StartCoroutine(setspeedDown(globalMoveSpd));
                }
            }
            else { //Speed up
                   //No longer braking;
                braking = false;
                //Set targetSpd to globalMaxSpd
                targetSpd = globalMaxSpd;

                //If the couritine is not busy, and the globalSpd has not reached globalMaxSpd
                if (!occupied && (globalMoveSpd < globalMaxSpd))
                {
                    //Pass in speed at which acceleration has tarted;
                    StartCoroutine(setspeedUp(globalMoveSpd));
                }


            }
        }
    }

    public void incrementScore()
    {
        if(state == GameState.normalPlay || state == GameState.wallEvent)
        {
            //Add to score every second
            if (timeToStart >= 0.08)
            {
				distance += globalMoveSpd;
				score += (int)(globalMoveSpd / brakingSpd) * multiplier;
                timeToStart = 0;
            }

            timeToStart += Time.deltaTime;
        }
        
    }

    public IEnumerator readyState(float seconds)
    {
        stateOccupied = true;

        //Initialize game objects and stats here
        init();

        yield return new WaitForSeconds(0.5f);

        GetReadyGUI.current.getReadyGUIAnim.SetBool("GetReady", true); //Play get ready animation

        yield return new WaitForSeconds(seconds);

        GetReadyGUI.current.getReadyGUIAnim.SetBool("GetReady", false); //Reset animation state

        score = 0; //Reset score again
		distance = 0;
		lastBrake = 0;
		lastSpeedBump = 0;
		multiplier = 1;

        //begin normal play state
        state = GameState.normalPlay;

        stateOccupied = false;
    }
    
	public IEnumerator restartState(float seconds)
	{

		stateOccupied = true;

		globalMaxSpd = globalMoveSpd = -50;

		player.GetComponent<CapsuleCollider>().enabled = false;
		player.GetComponent<Transform>().rotation = Quaternion.Euler (0.0f, 180.0f, 0.0f);

		yield return new WaitForSeconds(1f);

		player.GetComponent<CapsuleCollider>().enabled = true;

		globalMaxSpd = 20;
		globalMoveSpd = 20;

		disableAllClusters();

        stateOccupied = false;

		StartCoroutine(readyState(seconds));


	}

    public IEnumerator returnToTitle(float seconds)
    {

		stateOccupied = true;

        globalMaxSpd = globalMoveSpd = -50;

        player.GetComponent<CapsuleCollider>().enabled = false;
        player.GetComponent<Transform>().rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);

        yield return new WaitForSeconds(1f);

        player.GetComponent<CapsuleCollider>().enabled = true;

        globalMaxSpd = 20;
        globalMoveSpd = 20;

        disableAllClusters();

        state = GameState.menu;

        stateOccupied = false;
    }


    //Initialize game variables
    public void init()
    {
        score = 0;
		distance = 0;
		lastBrake = 0;
		lastSpeedBump = 0;
		multiplier = 1;
        eventCooldown = false;
        disableAllClusters();

    }

    //For debug, if using keyboard controls
    public bool usingKeyboardScheme()
    {
        return keyboardControls;
    }

    //For debug, if using keyboard controls
    public bool usingPhoneScheme()
    {
        return phoneControls;
    }

    //For debug, if using keyboard controls
    public bool usingMouseScheme()
    {
        return mouseControls;
    }

    //Save highscores, use when calling GM in other scripts
    public static void saveHighScore()
    {
        PlayerPrefs.SetInt("Highscore", gm.highScore);
    }

    //Load player preferences
    public void loadPrefs()
    {
        highScore = PlayerPrefs.GetInt("Highscore");
    }

    //Get highscore for GM when accessing gm from other scripts
    public int getHighscore()
    {
        return highScore;
    }

    //ResetHighscore, called by options screen in-game, from TitleScreenUIScript
    public void resetHighscore()
    {
        highScore = 0;
        PlayerPrefs.SetInt("Highscore", gm.highScore);

    }

    //Get wall event selection, value only changed in GameManager script
    public int getwallEvent_selection()
    {
        return wallEvent_selection;
    }


    public void handleStateChanges()
    {
        //If gameover flag raised by player object, set state to gameover;
        if (gameOver)
        {
            state = GameState.gameOver;
        }

        //Game over, set movespeeds to 0;
        if (gameOver)
        {
            globalMoveSpd = 0;
        }

        //If gameover, press R to start game again;
        if (state == GameState.gameOver && Input.GetKey("r"))
        {
            state = GameState.normalPlay;
        }

        if (state == GameState.ready)
        {
            if (!stateOccupied)
            {
                StartCoroutine(readyState(delayBeforeReady));
            }
        }

        if (state == GameState.waiting)
        {
            if (!stateOccupied)
            {
                StartCoroutine(restartState(delayBeforeReady));
            }
        }

        if (state == GameState.returnToTitle)
        {
            if (!stateOccupied)
            {
                StartCoroutine(returnToTitle(delayBeforeReady));
            }
        }

        if (state == GameState.gameOver)
        {
            if (score > highScore)
            {
                highScore = score;
                saveHighScore();
            }


        }

        //////////////////EVENT BLOCK/////////////////////
        //First wall event will occur when score is greater than 200, and is not already during a wallevent
		if (state != GameState.wallEvent && (distance >= firstEvent_threshold) && !eventCooldown)
        {
            eventCooldown = true;
			distance_EventOccur = distance;
            reoccurThreshold = Random.Range(min_EventThreshold, max_EventThreshold);
            wallEvent_selection = Random.Range(1, 3);
            state = GameState.wallEvent;
        }

        //For reoccurring events, reset cooldown when state is in normalPlay, and target difference in score between last occurrence and current score is reached
        if (state == GameState.normalPlay && (distance - distance_EventOccur) >= reoccurThreshold && eventCooldown)
        {
            eventCooldown = false;
        }
        ////////////////////////////////////////////////////


        //mulitplier and difficulty handling
        if ((distance - lastBrake) > 1000)
        {
            multiplier = 2;
        }
        if ((distance - lastBrake) > 2000)
        {
            multiplier = 3;
        }
        if ((distance - lastBrake) > 3000)
        {
            multiplier = 4;
        }

        //every 10000 increase the global max speed by 2
        if ((distance - lastSpeedBump) / 10000 >= 1)
        {
            globalMaxSpd += 2;
            lastSpeedBump = distance;
        }

        globalMaxSpd = Mathf.Clamp(globalMaxSpd, 20, 30);
    }

	public static void turnOffAds()
	{

		gm.adcontroller.hideBannerAd ();

	}

	public static void getNewAds()
	{
		if (gm.adcontroller.AdFlag ()) {
			gm.adcontroller.requestNewBannerAd ();
			gm.adcontroller.hideBannerAd();
			gm.adcontroller.requestNewInterstitialAd ();
		}
	}

	public IEnumerator showAd(float time)
	{
		yield return new WaitForSeconds (time);

		if (gm.adcontroller.adIsLoaded ()) {
			gm.adcontroller.showIntAd ();
		}

	}

	public void incAdCount()
	{
		adCount++;
	}

	public int getAdCount()
	{
		return adCount;
	}

	void OnApplicationQuit() 
	{
		adcontroller.destroyBannerAd ();
		adcontroller.destroyIntAd ();
	}
}