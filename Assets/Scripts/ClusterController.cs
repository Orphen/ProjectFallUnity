﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClusterController : MonoBehaviour {

    //Array of cluster's objects, size set in inspector;
    public GameObject[] cluster;
	public GameObject lastObject;

    //Clear flag, when all objects are off screen;
    private bool allInActive;

	// Use this for initialization
	void Start () {
        allInActive = false;
	}
	
	// Update is called once per frame
	void Update () {

        foreach(GameObject o in cluster)
        {
            Vector3 screenPoint = Camera.main.WorldToViewportPoint(o.transform.position);
            bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;

            if (onScreen && (o.transform.position.y < GameManager.gm.player.transform.position.y))
            {
                //Check all objects, if any are on screen do not clear cluster;
                //clear = false;
            }
            else if (!onScreen && (o.transform.position.y > GameManager.gm.player.transform.position.y))
            {
                //When all objects are off screen, clear cluster;
                //clear = true;
                o.SetActive(false);
            }

        }

        int objectCount = 0;
        foreach (GameObject o in cluster)
        {
            
            if(!o.activeInHierarchy)
            {
                objectCount++;
            }
            else
            {
                objectCount = 0;
            }

            if(objectCount == cluster.Length)
            {
                this.gameObject.SetActive(false);
            }

            
        }
		
	}

    void OnEnable()
    {
        foreach (GameObject o in cluster)
        {
            o.SetActive(true);
        }
    }
}
