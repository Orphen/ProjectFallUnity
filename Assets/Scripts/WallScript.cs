﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallScript : MonoBehaviour {

	public float wallSpeed;

	//End positions
    private Vector3 leftEnd, rightEnd;

    //Start positions
    private Vector3 leftStart, rightStart;

    //Close, Open bools
    bool open = false;

    //Flag if a wall selection has been made, do not make multiple selections per event
    bool selected = false;
    int selection;


    

	// Use this for initialization
	void Start () {
        //End positions, towards middle of screen
        leftEnd = new Vector3(1.35f, transform.position.y, transform.position.z);
        rightEnd = new Vector3(-1.35f, transform.position.y, transform.position.z);

        //Starting positions, edge of screen
        leftStart = new Vector3(3.75f, transform.position.y, transform.position.z);
        rightStart = new Vector3(-3.75f, transform.position.y, transform.position.z);
        

    }
	
	// Update is called once per frame
	void Update () {
        if(GameManager.gm.state == GameManager.GameState.ready)
        {
            resetWallPositions(); //Assign walls starting position to make sure they are correctly in their position 100%
        }

        //When in a reset state, move walls back to starting positions
        if(GameManager.gm.state == GameManager.GameState.waiting || GameManager.gm.state == GameManager.GameState.returnToTitle)
        {
            if (this.tag == "LeftWall")
            {
                transform.position = Vector3.MoveTowards(transform.position, leftStart, (Time.deltaTime * -GameManager.gm.getGlobalMoveSpeed() / 15));
            }
            else //right wall
            {
                transform.position = Vector3.MoveTowards(transform.position, rightStart, (Time.deltaTime * -GameManager.gm.getGlobalMoveSpeed() / 15));
            }

            open = false;
        }

        if(GameManager.gm.state == GameManager.GameState.wallEvent)
        {

            print(GameManager.gm.getwallEvent_selection());
          

            if (GameManager.gm.getwallEvent_selection() == 1)
            {
                moveBothWalls();
            }
            else if(GameManager.gm.getwallEvent_selection() == 2)
            {
                moveLeftWall();
            }
            else //selection 3
            {
                moveRightWall();
            }
            
        }
        
	}

    public void resetWallPositions()
    {
        
        if(this.tag == "LeftWall")
        {
            transform.position = leftStart;
        }
        else //rightWall
        {
            transform.position = rightStart;
        }
    }

    public void resetToNormalPlayState()
    {
        selected = false;
        GameManager.gm.state = GameManager.GameState.normalPlay;
    }

    public void moveBothWalls()
    {
        //Begin wall event, move walls toward center of screen;//
        if (this.tag == "LeftWall" && !open)
        {
            transform.position = Vector3.MoveTowards(transform.position, leftEnd, (Time.deltaTime * GameManager.gm.getGlobalMoveSpeed() / wallSpeed));
        }
        else if (this.tag == "RightWall" && !open) //rightWall
        {
            transform.position = Vector3.MoveTowards(transform.position, rightEnd, (Time.deltaTime * GameManager.gm.getGlobalMoveSpeed() / wallSpeed));
        }

        //End wall event, toggle flag to move walls towards edge of screen to start position//
        if (this.tag == "LeftWall" && transform.position.x <= leftEnd.x) //Left wall has reached end position, return to starting position
        {
            open = true;

        }
        else if (this.tag == "RightWall" && transform.position.x >= rightEnd.x) //Right wall has reached end position, now return to starting position
        {
            open = true;

        }

        //Ending wall event, if open flag toggled, move walls to edge of screen to starting position//
        if (open) //Return walls to starting position
        {
            if (this.tag == "LeftWall")
            {
                transform.position = Vector3.MoveTowards(transform.position, leftStart, (Time.deltaTime * GameManager.gm.getGlobalMoveSpeed() / wallSpeed));
            }
            else //right wall
            {
                transform.position = Vector3.MoveTowards(transform.position, rightStart, (Time.deltaTime * GameManager.gm.getGlobalMoveSpeed() / wallSpeed));
            }
        }

        //Walls have returned to start position, return to normal play state//
        if (this.tag == "LeftWall" && transform.position.x >= leftStart.x && open)
        {
            open = false;

            resetToNormalPlayState();

        }

        else if (this.tag == "rightWall" && transform.position.x <= rightStart.x && open)
        {
            open = false;

            resetToNormalPlayState();

        }
    }

    public void moveLeftWall()
    {
        if (this.tag == "LeftWall" && !open)
        {
            transform.position = Vector3.MoveTowards(transform.position, leftEnd, (Time.deltaTime * GameManager.gm.getGlobalMoveSpeed() / wallSpeed));
        }

        if (this.tag == "LeftWall" && transform.position.x <= leftEnd.x) //Left wall has reached end position, return to starting position
        {
            open = true;

        }

        if (open) //Return walls to starting position
        {
            if (this.tag == "LeftWall")
            {
                transform.position = Vector3.MoveTowards(transform.position, leftStart, (Time.deltaTime * GameManager.gm.getGlobalMoveSpeed() / wallSpeed));
            }
            else //right wall
            {
                transform.position = Vector3.MoveTowards(transform.position, rightStart, (Time.deltaTime * GameManager.gm.getGlobalMoveSpeed() / wallSpeed));
            }
        }


        //Wall has reached the starting position, end event state and return to normal play state;
        if (this.tag == "LeftWall" && transform.position.x >= leftStart.x && open)
        {
            open = false;

            resetToNormalPlayState();

        }
    }

    public void moveRightWall()
    {
        if (this.tag == "RightWall" && !open) //rightWall
        {
            transform.position = Vector3.MoveTowards(transform.position, rightEnd, (Time.deltaTime * GameManager.gm.getGlobalMoveSpeed() / wallSpeed));
        }

        if (this.tag == "RightWall" && transform.position.x >= rightEnd.x) //Right wall has reached end position, now return to starting position
        {
            open = true;

        }

        if (open) //Return walls to starting position
        {
            if (this.tag == "LeftWall")
            {
                transform.position = Vector3.MoveTowards(transform.position, leftStart, (Time.deltaTime * GameManager.gm.getGlobalMoveSpeed() / wallSpeed));
            }
            else //right wall
            {
                transform.position = Vector3.MoveTowards(transform.position, rightStart, (Time.deltaTime * GameManager.gm.getGlobalMoveSpeed() / wallSpeed));
            }
        }

        if (this.tag == "rightWall" && transform.position.x <= rightStart.x && open)
        {
            open = false;

            resetToNormalPlayState();

        }
    }


}
