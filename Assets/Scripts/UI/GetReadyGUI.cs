﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class used for referencing the GUI for the GET READY text animator
public class GetReadyGUI : MonoBehaviour {

    //For global referencing
    public static GetReadyGUI current;

    [SerializeField]
    public Animator getReadyGUIAnim; //set in inspector

    // Use this for initialization
    void Start()
    {
        current = this;

        if (getReadyGUIAnim == null)
        {
            Debug.LogError("no GetReadyGUI referenced");
        }
    }
}
