﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverUIScript : MonoBehaviour {

    //UI Slide speed//
    public float speedOn, speedOff; //set in inspector

    //UI Elements// Set in inspector
    [SerializeField]
    private RectTransform gameText;
    [SerializeField]
    private RectTransform overText;
    [SerializeField]
    private RectTransform restartButton, titleButton;
    [SerializeField]
    private RectTransform highScoreText;
    private Text highScore_text;

    //OnScreenTextPositions// Set in inspector
    [SerializeField]
    private RectTransform Game_OnScreenPos;
    [SerializeField]
    private RectTransform Over_OnScreenPos;
    [SerializeField]
    private RectTransform restart_OnScreenPos, titleBttn_OnScreenPos;
    [SerializeField]
    private RectTransform highScore_OnScreenPos;

    //OffScreenTextPositions// Set in inspector
    [SerializeField]
    private RectTransform Game_OffScreenPos;
    [SerializeField]
    private RectTransform Over_OffScreenPos;
    [SerializeField]
    private RectTransform restart_OffScreenPos; //Shared with the Highscore offscreen text location

    //TitleScreenObject to set active //set in inspector
    [SerializeField]
    private RectTransform titleScreen;

    //IEnumerator occupied;
    public bool occupied;

    //restart game button action
    public void restartGame()
    {
        if(!occupied)
        {
            StartCoroutine(slideOutUIElements());

			if(GameManager.gm.adcontroller.AdFlag() && ((GameManager.gm.getAdCount() % GameManager.gm.adCooldown) == 0))
			{
				GameManager.turnOffAds();
				GameManager.getNewAds();
			}
			GameManager.gm.incAdCount ();
        }
    }

    //Menu game button action
    public void returnToTitle()
    {
        if(!occupied)
        {
            StartCoroutine(slideOutUI_returnToTitle());
			if(GameManager.gm.adcontroller.AdFlag() && ((GameManager.gm.getAdCount() % GameManager.gm.adCooldown) == 0))
			{
				GameManager.turnOffAds();
				GameManager.getNewAds();
			}
			GameManager.gm.incAdCount ();
        }
    }

    public void Update()
    {
        if (GameManager.gm.state != GameManager.GameState.gameOver)
        {
            this.gameObject.SetActive(false);
        }
    }

    public void OnEnable()
    {
        highScore_text = highScoreText.GetComponent<Text>();
        UpdateHighScoreText();
        StartCoroutine(slideInUIElements());
    }


    public IEnumerator slideInUIElements() //Slide UI elements onscreen;
    {

        occupied = true;

        float timeToStart = Time.time; //Move in based on factor of time, not frame rate
        while(gameText.localPosition != Game_OnScreenPos.localPosition && overText.localPosition != Over_OnScreenPos.localPosition) 
        {
            gameText.localPosition = Vector3.Lerp(gameText.localPosition, Game_OnScreenPos.localPosition, speedOn * (Time.time - timeToStart));
            overText.localPosition = Vector3.Lerp(overText.localPosition, Over_OnScreenPos.localPosition, speedOn * (Time.time - timeToStart));
            
            yield return null;
        }

        timeToStart = Time.time;

        while (restartButton.localPosition != restart_OnScreenPos.localPosition && titleButton.localPosition != titleBttn_OnScreenPos.localPosition)
        {
            restartButton.localPosition = Vector3.Lerp(restartButton.localPosition, restart_OnScreenPos.localPosition, speedOff * (Time.time - timeToStart));
            titleButton.localPosition = Vector3.Lerp(titleButton.localPosition, titleBttn_OnScreenPos.localPosition, speedOff * (Time.time - timeToStart));

            yield return null;
        }


        while (highScoreText.localPosition != highScore_OnScreenPos.localPosition)
        {
           highScoreText.localPosition = Vector3.Lerp(highScoreText.localPosition, highScore_OnScreenPos.localPosition, speedOff/2 * (Time.time - timeToStart));

            yield return null;
        }


        occupied = false;


    }

    private IEnumerator slideOutUIElements() //Slide UI elements onscreen;
    {
        occupied = true;

        float timeToStart = Time.time; //Move in based on factor of time, not frame rate
        while (gameText.localPosition != Game_OffScreenPos.localPosition && overText.localPosition != Over_OffScreenPos.localPosition)
        {
            gameText.localPosition = Vector3.Lerp(gameText.localPosition, Game_OffScreenPos.localPosition, speedOff * (Time.time - timeToStart));
            overText.localPosition = Vector3.Lerp(overText.localPosition, Over_OffScreenPos.localPosition, speedOff * (Time.time - timeToStart));
            restartButton.localPosition = Vector3.Lerp(restartButton.localPosition, restart_OffScreenPos.localPosition, speedOff * (Time.time - timeToStart));
            titleButton.localPosition = Vector3.Lerp(titleButton.localPosition, restart_OffScreenPos.localPosition, speedOff * (Time.time - timeToStart));
            highScoreText.localPosition = Vector3.Lerp(restartButton.localPosition, restart_OffScreenPos.localPosition, speedOff * (Time.time - timeToStart));

            yield return null;
        }

        restartGameState();

        occupied = false;
    }

    //Return to title call, used for button press
    private IEnumerator slideOutUI_returnToTitle() //Slide UI elements onscreen;
    {
        occupied = true;

        float timeToStart = Time.time; //Move in based on factor of time, not frame rate
        while (gameText.localPosition != Game_OffScreenPos.localPosition && overText.localPosition != Over_OffScreenPos.localPosition)
        {
            gameText.localPosition = Vector3.Lerp(gameText.localPosition, Game_OffScreenPos.localPosition, speedOff * (Time.time - timeToStart));
            overText.localPosition = Vector3.Lerp(overText.localPosition, Over_OffScreenPos.localPosition, speedOff * (Time.time - timeToStart));
            restartButton.localPosition = Vector3.Lerp(restartButton.localPosition, restart_OffScreenPos.localPosition, speedOff * (Time.time - timeToStart));
            titleButton.localPosition = Vector3.Lerp(titleButton.localPosition, restart_OffScreenPos.localPosition, speedOff * (Time.time - timeToStart));
            highScoreText.localPosition = Vector3.Lerp(restartButton.localPosition, restart_OffScreenPos.localPosition, speedOff * (Time.time - timeToStart));

            yield return null;
        }

        moveToTitleState();

        occupied = false;
    }

    private void restartGameState() //Reset gameOver UI elements, then transition to waiting state -> ready state;
    {
        gameText.localPosition = Game_OffScreenPos.localPosition;
        overText.localPosition = Over_OffScreenPos.localPosition;
        restartButton.localPosition = restart_OffScreenPos.localPosition;
        highScoreText.localPosition = restart_OffScreenPos.localPosition;

        GameManager.gm.gameOver = false;
        GameManager.gm.prevState = GameManager.gm.state;
		GameManager.gm.state = GameManager.GameState.waiting;

    }

    private void moveToTitleState() //Reset gameOver UI elements, then transition to title menu state - > menu state;
    {
        gameText.localPosition = Game_OffScreenPos.localPosition;
        overText.localPosition = Over_OffScreenPos.localPosition;
        restartButton.localPosition = restart_OffScreenPos.localPosition;
        highScoreText.localPosition = restart_OffScreenPos.localPosition;

        GameManager.gm.gameOver = false;
        GameManager.gm.prevState = GameManager.gm.state;
        GameManager.gm.state = GameManager.GameState.returnToTitle;
    }



    public void UpdateHighScoreText()
    {
        highScore_text.text = "HighScore: " + GameManager.gm.getHighscore().ToString();
    }




}
