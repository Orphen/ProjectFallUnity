﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TitleScreenUIScript : MonoBehaviour {

    //UI Slide speed
    public float speed; //set in inspector

    //OnScreen UI elements// For sliding text on screen, Set in inspector
    [SerializeField]
    private RectTransform title;
    [SerializeField]
    private RectTransform startButton, optionsButton;
    [SerializeField]
    private RectTransform options; //Options panel
    [SerializeField]
    private Slider volSlider; //volume slider
    [SerializeField]
    private Text highscoreText; //highscore value shown on options panel


    //Offscreen positions// For sliding text off screen, Set in inspector 
    [SerializeField]
    private RectTransform inActiveStartButton, inActiveTitle;
    [SerializeField]
    private RectTransform title_onScreen, start_onScreen, options_onScreen;

    //IENumerator occupied;
    private bool occupied;

    //Start game button action
    public void startGame()
    {

        if (!occupied)
        {
            StartCoroutine(slideOutUIElements());
        }
        
    }

    public void OnEnable()
    {
        if (!occupied && GameManager.gm != null)
        {
            StartCoroutine(slideInUIElements());
        }
    }

    public void openOptions()
    {
        options.gameObject.SetActive(true);
        highscoreText.text = "HIGHSCORE: " + GameManager.gm.getHighscore().ToString();
    }

    public void closeOptions()
    {
        options.gameObject.SetActive(false);
    }

    public void adjustVolume()
    {
        AudioListener.volume = volSlider.value;
    }

    public void resetHighScore()
    {
        GameManager.gm.resetHighscore();
        highscoreText.text = "HIGHSCORE: " + GameManager.gm.getHighscore().ToString();
    }
        

    IEnumerator slideOutUIElements()
    {
        occupied = true;
        float timeToStart = Time.time; //Move in based on factor of time, not frame rate
        while(title.localPosition != inActiveTitle.localPosition && startButton.localPosition != inActiveStartButton.localPosition)
        {
            title.localPosition = Vector3.Lerp(title.localPosition, inActiveTitle.localPosition, speed * (Time.time - timeToStart));
            startButton.localPosition = Vector3.Lerp(startButton.localPosition, inActiveStartButton.localPosition, speed * (Time.time - timeToStart));
            optionsButton.localPosition = Vector3.Lerp(optionsButton.localPosition, inActiveStartButton.localPosition, speed * (Time.time - timeToStart));

            yield return null;
        }

    
        GameManager.gm.prevState = GameManager.gm.state;
        GameManager.gm.state = GameManager.GameState.ready;

        occupied = false;

    }

    IEnumerator slideInUIElements()
    {
        occupied = true;
        float timeToStart = Time.time; //Move in based on factor of time, not frame rate

        while (title.localPosition != title_onScreen.localPosition && startButton.localPosition != start_onScreen.localPosition && optionsButton.localPosition != options_onScreen.localPosition)
        {
            title.localPosition = Vector3.Lerp(title.localPosition, title_onScreen.localPosition, speed * (Time.time - timeToStart));
            startButton.localPosition = Vector3.Lerp(startButton.localPosition, start_onScreen.localPosition, speed * (Time.time - timeToStart));
            optionsButton.localPosition = Vector3.Lerp(optionsButton.localPosition, options_onScreen.localPosition, speed * (Time.time - timeToStart));

            yield return null;
        }

        GameManager.gm.prevState = GameManager.gm.state;
        GameManager.gm.state = GameManager.GameState.menu;

        occupied = false;
    }

}
