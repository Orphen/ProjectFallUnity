﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreUI : MonoBehaviour {

    private Text scoreText;

    public static ScoreUI current;

	// Use this for initialization
	void Start () {

        current = this;

        scoreText = GetComponent<Text>();
        scoreText.text = "Score: " + GameManager.gm.getScore();
		
	}
	
	// Update is called once per frame
	void Update () {

        scoreText.text = "Score: " + GameManager.gm.getScore();
    }
}
