﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class MultiplierUI : MonoBehaviour {

	private Text multiplierText;

	public static MultiplierUI current;

	// Use this for initialization
	void Start () {

		current = this;

		multiplierText = GetComponent<Text>();
		multiplierText.text = "Multiplier: x" + GameManager.gm.getMultiplier();

	}

	// Update is called once per frame
	void Update () {

		multiplierText.text = "Mutliplier: x" + GameManager.gm.getMultiplier();
	}
}
