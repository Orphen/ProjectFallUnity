﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class Player : MonoBehaviour
{

	public AudioManager am;

	//Get reference to this object's transform/rigidbody for efficiency.
    private Transform myTransform;
    private Rigidbody myBody;
    public Animator animator;

    //////Break meter handling//////
    public float brakeDecRate, brakeIncRate, brakeMeterMax; //Inc and dec rates, max value, set in inspector
    [SerializeField]
    private float brakeMeterCur; //current Boost meter status value
    private bool recharging; //Indicate if all boost was used, turn off braking;
    private bool braking; //reference for when the player indicates braking.

    //variable for the amount of tilt
    public float tilt;

    private StatusIndicator brakeMeterUI; //Reference to status indicator script to set GUI elements.

    //Canvas for tap to UI checking
    public Canvas canvas; //Set in inspector




    // Use this for initialization
    void Start()
    {
        //Assign current game object's transform.
        myTransform = transform;
        myBody = gameObject.GetComponent<Rigidbody>();

        //Assign statusindicator script ref;
        brakeMeterUI = GetComponentInChildren<StatusIndicator>();

        brakeMeterCur = brakeMeterMax;
    }

    // Update is called once per frame
    void Update()
    {

        //Move towards center position when restarting or returning to titleScreen
        if (GameManager.gm.state == GameManager.GameState.waiting || GameManager.gm.state == GameManager.GameState.returnToTitle)
        {
            float step = 5f * Time.deltaTime;
            myBody.position = Vector3.MoveTowards(myBody.position, new Vector3(0, myBody.position.y, myBody.position.z), step);
        }
        //Allow player control only during testing and normalPlay
        if (GameManager.gm.state == GameManager.GameState.normalPlay || GameManager.gm.state == GameManager.GameState.test || GameManager.gm.state == GameManager.GameState.ready || GameManager.gm.state == GameManager.GameState.wallEvent)
        {


            if (GameManager.gm.usingKeyboardScheme())
            {
                useKeyboardControls();
            }
            else if (GameManager.gm.usingPhoneScheme())
            {
                usePhoneControls();
            }
            else if (GameManager.gm.usingMouseScheme())
            {
                useMouseControls();
            }
            else //Default to keyboard if none selected
            {
                useKeyboardControls();
            }
        }


        brakeMeterUI.setBrakeBar((int)brakeMeterCur, 100);


    }

    //Check if tapped screen position overlaps with any UI;
    private bool IsPointerOverUIObject(Canvas canvas, Vector2 screenPosition)
    {
        // Referencing this code for GraphicRaycaster https://gist.github.com/stramit/ead7ca1f432f3c0f181f
        // the ray cast appears to require only eventData.position.
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = screenPosition;

        GraphicRaycaster uiRaycaster = canvas.gameObject.GetComponent<GraphicRaycaster>();
        List<RaycastResult> results = new List<RaycastResult>();
        uiRaycaster.Raycast(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    //Hit an obstacle;
    void OnCollisionEnter()
    {
        GameManager.gm.diedAtState = GameManager.gm.state;
        GameManager.gm.gameOver = true;
		am.PlaySound ("Hit");
        //on death, stop player character movement
        myBody.velocity = new Vector3(0f, 0f, 0f);

		if(GameManager.gm.adcontroller.AdFlag() && ((GameManager.gm.getAdCount() % GameManager.gm.adCooldown) == 0))
		{
			GameManager.gm.StartCoroutine(GameManager.gm.showAd (2f));

			GameManager.gm.adcontroller.showBannerAd ();
		}
    }

    //Return truye false of recharging state
    public bool isRecharging()
    {
        return recharging;
    }

    public bool isBraking()
    {
        return braking;
    }

    public void useMouseControls()
    {
        /////////////MOUSE SCREEN CONTROLS//////////////

        //Rotates the player slightly in the direction of player movement
        myBody.rotation = Quaternion.Euler(0.0f, 180.0f + (myBody.velocity.x * tilt), 0.0f);

        //Only move player when taps are on the bottom half of the screen.
        if (Input.GetMouseButton(0) && (Input.mousePosition.y < Screen.height / 2))
        {
            //Move player left tapping left half of screen
            if (Input.mousePosition.x < Screen.width / 2)
            {
                myBody.velocity = new Vector3(1f, 0f, 0f) * 5f;
            }
            //Move player right tapping right half of screen
            else if (Input.mousePosition.x > Screen.width / 2)
            {
                myBody.velocity = new Vector3(-1f, 0f, 0f) * 5f;
            }
        }
        else //Stop moving
        {
            myBody.velocity = Vector3.zero;
        }

        //Brake if tapping top half of screen
        if (Input.GetMouseButton(0) && (Input.mousePosition.y > Screen.height / 2) && !recharging)
        {
            braking = true;
            animator.SetBool("Slowdown", true);
            animator.SetBool("Falling", false);
            if (brakeMeterCur <= 0)
            { //If brake usage reaches below 0, stop decrementing.
                brakeMeterCur = 0;
                recharging = true;
            }
            else
            {
                brakeMeterCur -= brakeDecRate;
            }

        }
        else { //Speed up
            braking = false;
            animator.SetBool("Falling", true);
            animator.SetBool("Slowdown", false);
            if (brakeMeterCur >= brakeMeterMax)
            { //If brake recharge reaches max, top off and stay there
                brakeMeterCur = 100f;
                if (recharging)
                {
                    recharging = false;
                }

            }
            else
            {
                braking = false;
                brakeMeterCur += brakeIncRate;
            }

        }
        ////////////////////////////////////////////////
    }

    public void useKeyboardControls()
    {
        ///////////KEYBOARD CONTROLS///////////////
        //Move Player Left
        if (Input.GetKey("left"))
        {
            //transform.position = new Vector3(transform.position.x + 0.1f, transform.position.y, transform.position.z);
            myBody.velocity = new Vector3(1f, 0f, 0f) * 5f;
        }
        //Move Player right;
        else if (Input.GetKey("right"))
        {
            //transform.position = new Vector3(transform.position.x - 0.1f, transform.position.y, transform.position.z);
            myBody.velocity = new Vector3(-1f, 0f, 0f) * 5f;
        }
        else
        {
            //Stop moving
            myBody.velocity = Vector3.zero;
        }


        //Rotates the player slightly in the direction of player movement
        myBody.rotation = Quaternion.Euler(0.0f, 180.0f + (myBody.velocity.x * tilt), 0.0f);

        //Slow down
        if (Input.GetKey("space") && !recharging)
        {
            braking = true;
            animator.SetBool("Slowdown", true);
            animator.SetBool("Falling", false);
            if (brakeMeterCur <= 0)
            { //If brake usage reaches below 0, stop decrementing.
                brakeMeterCur = 0;
                recharging = true;
            }
            else {
                brakeMeterCur -= brakeDecRate;
            }

        }
        else
        { //Speed up
            braking = false;
            animator.SetBool("Falling", true);
            animator.SetBool("Slowdown", false);
            if (brakeMeterCur >= brakeMeterMax)
            { //If brake recharge reaches max, top off and stay there
                brakeMeterCur = 100f;
                if (recharging)
                {
                    recharging = false;
                }

            }
            else
            {
                braking = false;
                brakeMeterCur += brakeIncRate;
            }

        }
    }

    public void usePhoneControls()
    {
        /////////////PHONE SCREEN CONTROLS//////////////

        //Only move player when taps are on the bottom half of the screen.
        if (Input.touchCount > 0 && (Input.GetTouch(0).position.y < Screen.height / 2))
        {
            //handleLeftAndRightMovement();
            newMovement();
        }
        else //Stop moving
        {
            myBody.velocity = Vector3.zero;
        }

        //Brake if tapping top half of screen
        if (Input.touchCount > 0 && (Input.GetTouch(0).position.y > Screen.height / 4) && !recharging)
        {
            braking = true;
            animator.SetBool("Slowdown", true);
            animator.SetBool("Falling", false);
            if (brakeMeterCur <= 0)
            { //If brake usage reaches below 0, stop decrementing.
                brakeMeterCur = 0;
                recharging = true;
            }
            else
            {
                brakeMeterCur -= brakeDecRate;
            }

            //handleLeftAndRightMovement();
            newMovement();

        }
        else { //Speed up
            braking = false;
            animator.SetBool("Falling", true);
            animator.SetBool("Slowdown", false);
            if (brakeMeterCur >= brakeMeterMax)
            { //If brake recharge reaches max, top off and stay there
                brakeMeterCur = 100f;
                if (recharging)
                {
                    recharging = false;
                }

            }
            else
            {
                braking = false;
                brakeMeterCur += brakeIncRate;
            }

            //handleLeftAndRightMovement();
            newMovement();
        }
        ////////////////////////////////////////////////
    }

    public void handleLeftAndRightMovement()
    {
        //Move player left tapping left half of screen
        if (Input.GetTouch(0).position.x < Screen.width / 2)
        {
            myBody.velocity = new Vector3(1f, 0f, 0f) * 5f;
        }
        //Move player right tapping right half of screen
        else if (Input.GetTouch(0).position.x > Screen.width / 2)
        {
            myBody.velocity = new Vector3(-1f, 0f, 0f) * 5f;
        }
    }

    public void newMovement()
    {
        Ray ray;
        float z_plane_of_2d_game = 0;
        Vector3 pos_at_z_0;

         ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

        //If tap registered
        if (Input.touchCount > 0)
        {
            //Translate screen tapped location to in-game coordinates
            ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            pos_at_z_0 = ray.origin + ray.direction * (z_plane_of_2d_game - ray.origin.z) / ray.direction.z;

            //Depending on tapped location, if the player object has not reached destination, rotate towards tapped point
            if (Input.GetTouch(0).position.x < Screen.width / 2 && (myTransform.position.x != pos_at_z_0.x)) //Moving right
            {
                myBody.rotation = Quaternion.Euler(0.0f, 180.0f + (5 * tilt), 0.0f);
            }
            else if (Input.GetTouch(0).position.x > Screen.width / 2 && (myTransform.position.x != pos_at_z_0.x))//Moving left
            {
                myBody.rotation = Quaternion.Euler(0.0f, 180.0f + (-5 * tilt), 0.0f);
            }
            else //Not tilted
            {
                myBody.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
            }

            
            //Movetowards tapped location
            myTransform.position = Vector3.MoveTowards(myTransform.position, new Vector3(pos_at_z_0.x, myTransform.position.y, myTransform.position.z), Time.deltaTime * 3f);
        }

    }
}


