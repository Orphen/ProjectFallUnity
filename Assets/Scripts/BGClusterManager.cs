using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGClusterManager : MonoBehaviour {

	//Array of cluster objects, size set in inspector;
	public GameObject[] cluster;

	public GameObject cluster1, cluster2, cluster3;

	//Keep track of current active cluster;
	bool activeCluster;

	// Use this for initialization
	void Start () {

		cluster1 = findNewCluster();
		cluster2 = findNewCluster();
		cluster3 = findNewCluster();

		Vector3 c1 = cluster1.GetComponent<Transform> ().position;
		Vector3 c2 = new Vector3 (0, cluster1.GetComponent<ClusterController>().lastObject.transform.position.y - 6, c1.z);
		cluster2.transform.position = c2;
		Vector3 c3 = new Vector3 (0, cluster2.GetComponent<ClusterController>().lastObject.transform.position.y - 6, c1.z);
		cluster3.transform.position = c3;
	}

	// Update is called once per frame
	void Update() {

		//If all clusters disabled, find new cluster
		if (cluster1Disabled())
		{
			if (GameManager.gm.state != GameManager.GameState.gameOver) {
				cluster1 = cluster2;
				cluster2 = cluster3;
				cluster3 = findNewCluster ();

				cluster3.transform.position = new Vector3( 0, cluster2.GetComponent<ClusterController>().lastObject.transform.position.y - 6, cluster2.transform.position.z);
			}

		}
    
	}

	public bool cluster1Disabled()
	{
		if (!cluster1.activeInHierarchy)
			return true; //all objects returned inactive, return true;
		else
			return false;
	}

	public GameObject findNewCluster()
	{
		//Find new cluster
		GameObject target;
		int selection = Random.Range(0, cluster.Length);
		target = cluster [selection];

		while(!isclusterDisabled(target))
		{
			selection = Random.Range(0, cluster.Length);
			target = cluster [selection];
		}

		//enable the new cluster
		cluster[selection].SetActive(true);

		return cluster[selection];
	}

	public bool isclusterDisabled(GameObject gameObject)
	{
		if (!gameObject.activeInHierarchy) {
			return true;
		} else {
			return false;
		}
	}

}