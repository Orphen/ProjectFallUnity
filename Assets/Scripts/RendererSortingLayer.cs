﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RendererSortingLayer : MonoBehaviour 
{
	//The objects sotring layer and order in layer
	public string sortingLayer;
	public int orderInLayer;

	void Awake(){
		SetSortingLayer ();
	}
			
	//Context menu to update the sorting layer and order in layer
	[ContextMenu ("Update sorting layer settings")]
	void UpdateSortingLayerSettings(){
		SetSortingLayer();
	}

	//sets the sorting and order in layer of the renderer
	private void SetSortingLayer()
	{
		Renderer rend = GetComponent<Renderer>();
		rend.sortingLayerName = sortingLayer;
		rend.sortingOrder = orderInLayer;
	}

}
