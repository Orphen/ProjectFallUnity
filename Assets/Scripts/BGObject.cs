using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGObject : MonoBehaviour {

    //Get reference to this object's transform/rigidbody for efficiency.
    private Transform myTransform;
    private Vector3 startPosition;
    private Rigidbody myBody;

    // Use this for initialization
    void Start()
    {
        //Assign current game object's transform.
        myTransform = transform;
        myBody = gameObject.GetComponent<Rigidbody>();
        startPosition = transform.localPosition;
    }

    // Update is called once per frame
    void Update () {
        
        //Update velocity based on value held by game manager;
        myBody.velocity = new Vector3(0f, 0.25f, 0f) * GameManager.gm.getGlobalMoveSpeed();

    }

    void OnDisable()
    {
		myTransform.localPosition = startPosition;
    }
}
